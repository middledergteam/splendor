﻿namespace Splendor.Core.Enumeraciones
{
    public enum Fase
    {
        Comienzo = 1,
        EscogerGemas,
        Comprar,
        RecibirNoble,
        FinDeTurno,
        FinDePartida
    }
}
