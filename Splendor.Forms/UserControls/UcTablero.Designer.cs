﻿namespace Splendor.Forms.UserControls
{
    partial class UcTablero
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.TlpGeneral = new System.Windows.Forms.TableLayoutPanel();
            this.Flp3 = new System.Windows.Forms.FlowLayoutPanel();
            this.Flp2 = new System.Windows.Forms.FlowLayoutPanel();
            this.Flp1 = new System.Windows.Forms.FlowLayoutPanel();
            this.FlpTrasera2 = new System.Windows.Forms.FlowLayoutPanel();
            this.FlpTrasera1 = new System.Windows.Forms.FlowLayoutPanel();
            this.FlpTrasera3 = new System.Windows.Forms.FlowLayoutPanel();
            this.ucCarta10 = new Splendor.Forms.UserControls.UcCarta();
            this.ucCarta11 = new Splendor.Forms.UserControls.UcCarta();
            this.ucCarta12 = new Splendor.Forms.UserControls.UcCarta();
            this.ucCarta13 = new Splendor.Forms.UserControls.UcCarta();
            this.ucCarta6 = new Splendor.Forms.UserControls.UcCarta();
            this.ucCarta7 = new Splendor.Forms.UserControls.UcCarta();
            this.ucCarta8 = new Splendor.Forms.UserControls.UcCarta();
            this.ucCarta9 = new Splendor.Forms.UserControls.UcCarta();
            this.ucCarta1 = new Splendor.Forms.UserControls.UcCarta();
            this.ucCarta3 = new Splendor.Forms.UserControls.UcCarta();
            this.ucCarta4 = new Splendor.Forms.UserControls.UcCarta();
            this.ucCarta5 = new Splendor.Forms.UserControls.UcCarta();
            this.Trasera2 = new Splendor.Forms.UserControls.UcCarta();
            this.Trasera1 = new Splendor.Forms.UserControls.UcCarta();
            this.Trasera3 = new Splendor.Forms.UserControls.UcCarta();
            this.TlpGeneral.SuspendLayout();
            this.Flp3.SuspendLayout();
            this.Flp2.SuspendLayout();
            this.Flp1.SuspendLayout();
            this.FlpTrasera2.SuspendLayout();
            this.FlpTrasera1.SuspendLayout();
            this.FlpTrasera3.SuspendLayout();
            this.SuspendLayout();
            // 
            // TlpGeneral
            // 
            this.TlpGeneral.ColumnCount = 2;
            this.TlpGeneral.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.TlpGeneral.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TlpGeneral.Controls.Add(this.Flp3, 1, 0);
            this.TlpGeneral.Controls.Add(this.Flp2, 1, 1);
            this.TlpGeneral.Controls.Add(this.Flp1, 1, 2);
            this.TlpGeneral.Controls.Add(this.FlpTrasera2, 0, 1);
            this.TlpGeneral.Controls.Add(this.FlpTrasera1, 0, 2);
            this.TlpGeneral.Controls.Add(this.FlpTrasera3, 0, 0);
            this.TlpGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TlpGeneral.Font = new System.Drawing.Font("Georgia", 9F);
            this.TlpGeneral.Location = new System.Drawing.Point(0, 0);
            this.TlpGeneral.Margin = new System.Windows.Forms.Padding(0);
            this.TlpGeneral.Name = "TlpGeneral";
            this.TlpGeneral.RowCount = 3;
            this.TlpGeneral.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TlpGeneral.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TlpGeneral.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TlpGeneral.Size = new System.Drawing.Size(800, 580);
            this.TlpGeneral.TabIndex = 0;
            // 
            // Flp3
            // 
            this.Flp3.Controls.Add(this.ucCarta10);
            this.Flp3.Controls.Add(this.ucCarta11);
            this.Flp3.Controls.Add(this.ucCarta12);
            this.Flp3.Controls.Add(this.ucCarta13);
            this.Flp3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Flp3.Location = new System.Drawing.Point(200, 0);
            this.Flp3.Margin = new System.Windows.Forms.Padding(0);
            this.Flp3.Name = "Flp3";
            this.Flp3.Size = new System.Drawing.Size(600, 193);
            this.Flp3.TabIndex = 3;
            // 
            // Flp2
            // 
            this.Flp2.Controls.Add(this.ucCarta6);
            this.Flp2.Controls.Add(this.ucCarta7);
            this.Flp2.Controls.Add(this.ucCarta8);
            this.Flp2.Controls.Add(this.ucCarta9);
            this.Flp2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Flp2.Location = new System.Drawing.Point(200, 193);
            this.Flp2.Margin = new System.Windows.Forms.Padding(0);
            this.Flp2.Name = "Flp2";
            this.Flp2.Size = new System.Drawing.Size(600, 193);
            this.Flp2.TabIndex = 2;
            // 
            // Flp1
            // 
            this.Flp1.Controls.Add(this.ucCarta1);
            this.Flp1.Controls.Add(this.ucCarta3);
            this.Flp1.Controls.Add(this.ucCarta4);
            this.Flp1.Controls.Add(this.ucCarta5);
            this.Flp1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Flp1.Location = new System.Drawing.Point(200, 386);
            this.Flp1.Margin = new System.Windows.Forms.Padding(0);
            this.Flp1.Name = "Flp1";
            this.Flp1.Size = new System.Drawing.Size(600, 194);
            this.Flp1.TabIndex = 0;
            // 
            // FlpTrasera2
            // 
            this.FlpTrasera2.Controls.Add(this.Trasera2);
            this.FlpTrasera2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FlpTrasera2.Location = new System.Drawing.Point(0, 193);
            this.FlpTrasera2.Margin = new System.Windows.Forms.Padding(0);
            this.FlpTrasera2.Name = "FlpTrasera2";
            this.FlpTrasera2.Size = new System.Drawing.Size(200, 193);
            this.FlpTrasera2.TabIndex = 5;
            // 
            // FlpTrasera1
            // 
            this.FlpTrasera1.Controls.Add(this.Trasera1);
            this.FlpTrasera1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FlpTrasera1.Location = new System.Drawing.Point(0, 386);
            this.FlpTrasera1.Margin = new System.Windows.Forms.Padding(0);
            this.FlpTrasera1.Name = "FlpTrasera1";
            this.FlpTrasera1.Size = new System.Drawing.Size(200, 194);
            this.FlpTrasera1.TabIndex = 6;
            // 
            // FlpTrasera3
            // 
            this.FlpTrasera3.Controls.Add(this.Trasera3);
            this.FlpTrasera3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FlpTrasera3.Location = new System.Drawing.Point(0, 0);
            this.FlpTrasera3.Margin = new System.Windows.Forms.Padding(0);
            this.FlpTrasera3.Name = "FlpTrasera3";
            this.FlpTrasera3.Size = new System.Drawing.Size(200, 193);
            this.FlpTrasera3.TabIndex = 7;
            // 
            // ucCarta10
            // 
            this.ucCarta10.Location = new System.Drawing.Point(10, 10);
            this.ucCarta10.Margin = new System.Windows.Forms.Padding(10);
            this.ucCarta10.Name = "ucCarta10";
            this.ucCarta10.Padding = new System.Windows.Forms.Padding(5);
            this.ucCarta10.Size = new System.Drawing.Size(129, 176);
            this.ucCarta10.TabIndex = 0;
            this.ucCarta10.Click += new System.EventHandler(this.CartaClick);
            // 
            // ucCarta11
            // 
            this.ucCarta11.Location = new System.Drawing.Point(159, 10);
            this.ucCarta11.Margin = new System.Windows.Forms.Padding(10);
            this.ucCarta11.Name = "ucCarta11";
            this.ucCarta11.Padding = new System.Windows.Forms.Padding(5);
            this.ucCarta11.Size = new System.Drawing.Size(129, 176);
            this.ucCarta11.TabIndex = 1;
            this.ucCarta11.Click += new System.EventHandler(this.CartaClick);
            // 
            // ucCarta12
            // 
            this.ucCarta12.Location = new System.Drawing.Point(308, 10);
            this.ucCarta12.Margin = new System.Windows.Forms.Padding(10);
            this.ucCarta12.Name = "ucCarta12";
            this.ucCarta12.Padding = new System.Windows.Forms.Padding(5);
            this.ucCarta12.Size = new System.Drawing.Size(129, 176);
            this.ucCarta12.TabIndex = 2;
            this.ucCarta12.Click += new System.EventHandler(this.CartaClick);
            // 
            // ucCarta13
            // 
            this.ucCarta13.Location = new System.Drawing.Point(457, 10);
            this.ucCarta13.Margin = new System.Windows.Forms.Padding(10);
            this.ucCarta13.Name = "ucCarta13";
            this.ucCarta13.Padding = new System.Windows.Forms.Padding(5);
            this.ucCarta13.Size = new System.Drawing.Size(129, 176);
            this.ucCarta13.TabIndex = 3;
            this.ucCarta13.Click += new System.EventHandler(this.CartaClick);
            // 
            // ucCarta6
            // 
            this.ucCarta6.Location = new System.Drawing.Point(10, 10);
            this.ucCarta6.Margin = new System.Windows.Forms.Padding(10);
            this.ucCarta6.Name = "ucCarta6";
            this.ucCarta6.Padding = new System.Windows.Forms.Padding(5);
            this.ucCarta6.Size = new System.Drawing.Size(129, 176);
            this.ucCarta6.TabIndex = 0;
            this.ucCarta6.Click += new System.EventHandler(this.CartaClick);
            // 
            // ucCarta7
            // 
            this.ucCarta7.Location = new System.Drawing.Point(159, 10);
            this.ucCarta7.Margin = new System.Windows.Forms.Padding(10);
            this.ucCarta7.Name = "ucCarta7";
            this.ucCarta7.Padding = new System.Windows.Forms.Padding(5);
            this.ucCarta7.Size = new System.Drawing.Size(129, 176);
            this.ucCarta7.TabIndex = 1;
            this.ucCarta7.Click += new System.EventHandler(this.CartaClick);
            // 
            // ucCarta8
            // 
            this.ucCarta8.Location = new System.Drawing.Point(308, 10);
            this.ucCarta8.Margin = new System.Windows.Forms.Padding(10);
            this.ucCarta8.Name = "ucCarta8";
            this.ucCarta8.Padding = new System.Windows.Forms.Padding(5);
            this.ucCarta8.Size = new System.Drawing.Size(129, 176);
            this.ucCarta8.TabIndex = 2;
            this.ucCarta8.Click += new System.EventHandler(this.CartaClick);
            // 
            // ucCarta9
            // 
            this.ucCarta9.Location = new System.Drawing.Point(457, 10);
            this.ucCarta9.Margin = new System.Windows.Forms.Padding(10);
            this.ucCarta9.Name = "ucCarta9";
            this.ucCarta9.Padding = new System.Windows.Forms.Padding(5);
            this.ucCarta9.Size = new System.Drawing.Size(129, 176);
            this.ucCarta9.TabIndex = 3;
            this.ucCarta9.Click += new System.EventHandler(this.CartaClick);
            // 
            // ucCarta1
            // 
            this.ucCarta1.Location = new System.Drawing.Point(10, 10);
            this.ucCarta1.Margin = new System.Windows.Forms.Padding(10);
            this.ucCarta1.Name = "ucCarta1";
            this.ucCarta1.Padding = new System.Windows.Forms.Padding(5);
            this.ucCarta1.Size = new System.Drawing.Size(129, 176);
            this.ucCarta1.TabIndex = 0;
            this.ucCarta1.Click += new System.EventHandler(this.CartaClick);
            // 
            // ucCarta3
            // 
            this.ucCarta3.Location = new System.Drawing.Point(159, 10);
            this.ucCarta3.Margin = new System.Windows.Forms.Padding(10);
            this.ucCarta3.Name = "ucCarta3";
            this.ucCarta3.Padding = new System.Windows.Forms.Padding(5);
            this.ucCarta3.Size = new System.Drawing.Size(129, 176);
            this.ucCarta3.TabIndex = 1;
            this.ucCarta3.Click += new System.EventHandler(this.CartaClick);
            // 
            // ucCarta4
            // 
            this.ucCarta4.BackColor = System.Drawing.Color.Transparent;
            this.ucCarta4.Location = new System.Drawing.Point(308, 10);
            this.ucCarta4.Margin = new System.Windows.Forms.Padding(10);
            this.ucCarta4.Name = "ucCarta4";
            this.ucCarta4.Padding = new System.Windows.Forms.Padding(5);
            this.ucCarta4.Size = new System.Drawing.Size(129, 176);
            this.ucCarta4.TabIndex = 2;
            this.ucCarta4.Click += new System.EventHandler(this.CartaClick);
            // 
            // ucCarta5
            // 
            this.ucCarta5.Location = new System.Drawing.Point(457, 10);
            this.ucCarta5.Margin = new System.Windows.Forms.Padding(10);
            this.ucCarta5.Name = "ucCarta5";
            this.ucCarta5.Padding = new System.Windows.Forms.Padding(5);
            this.ucCarta5.Size = new System.Drawing.Size(129, 176);
            this.ucCarta5.TabIndex = 3;
            this.ucCarta5.Click += new System.EventHandler(this.CartaClick);
            // 
            // Trasera2
            // 
            this.Trasera2.Location = new System.Drawing.Point(10, 10);
            this.Trasera2.Margin = new System.Windows.Forms.Padding(10);
            this.Trasera2.Name = "Trasera2";
            this.Trasera2.Padding = new System.Windows.Forms.Padding(5);
            this.Trasera2.Size = new System.Drawing.Size(129, 176);
            this.Trasera2.TabIndex = 5;
            this.Trasera2.Click += new System.EventHandler(this.CartaClick);
            // 
            // Trasera1
            // 
            this.Trasera1.Location = new System.Drawing.Point(10, 10);
            this.Trasera1.Margin = new System.Windows.Forms.Padding(10);
            this.Trasera1.Name = "Trasera1";
            this.Trasera1.Padding = new System.Windows.Forms.Padding(5);
            this.Trasera1.Size = new System.Drawing.Size(129, 176);
            this.Trasera1.TabIndex = 1;
            this.Trasera1.Click += new System.EventHandler(this.CartaClick);
            // 
            // Trasera3
            // 
            this.Trasera3.Location = new System.Drawing.Point(10, 10);
            this.Trasera3.Margin = new System.Windows.Forms.Padding(10);
            this.Trasera3.Name = "Trasera3";
            this.Trasera3.Padding = new System.Windows.Forms.Padding(5);
            this.Trasera3.Size = new System.Drawing.Size(129, 176);
            this.Trasera3.TabIndex = 4;
            this.Trasera3.Click += new System.EventHandler(this.CartaClick);
            // 
            // UcTablero
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TlpGeneral);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "UcTablero";
            this.Size = new System.Drawing.Size(800, 580);
            this.TlpGeneral.ResumeLayout(false);
            this.Flp3.ResumeLayout(false);
            this.Flp2.ResumeLayout(false);
            this.Flp1.ResumeLayout(false);
            this.FlpTrasera2.ResumeLayout(false);
            this.FlpTrasera1.ResumeLayout(false);
            this.FlpTrasera3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel TlpGeneral;
        private System.Windows.Forms.FlowLayoutPanel Flp1;
        private UcCarta ucCarta1;
        private UcCarta Trasera1;
        private UcCarta Trasera2;
        private UcCarta Trasera3;
        private System.Windows.Forms.FlowLayoutPanel Flp3;
        private UcCarta ucCarta10;
        private UcCarta ucCarta11;
        private UcCarta ucCarta12;
        private UcCarta ucCarta13;
        private System.Windows.Forms.FlowLayoutPanel Flp2;
        private UcCarta ucCarta6;
        private UcCarta ucCarta7;
        private UcCarta ucCarta8;
        private UcCarta ucCarta9;
        private UcCarta ucCarta3;
        private UcCarta ucCarta4;
        private UcCarta ucCarta5;
        private System.Windows.Forms.FlowLayoutPanel FlpTrasera2;
        private System.Windows.Forms.FlowLayoutPanel FlpTrasera1;
        private System.Windows.Forms.FlowLayoutPanel FlpTrasera3;
    }
}
