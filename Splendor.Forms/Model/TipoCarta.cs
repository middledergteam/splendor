﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Splendor.Forms.Model
{
    public enum EstadoCarta
    {
        Empty = 0,
        Desarrollo = 1,
        Trasera = 2
    }
}
